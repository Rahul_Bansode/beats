package bEATSpLAYER;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.awt.event.ActionEvent;
import java.awt.Toolkit;
import javax.swing.ImageIcon;

public class DeletePlayList extends JFrame {

	private JPanel contentPane;
	private JTextField deletepl;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DeletePlayList frame = new DeletePlayList("Rahul Bansode");
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public DeletePlayList(String Name) {
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\sai1\\Downloads\\bEATS.png"));
		setTitle("DeletePlayist Page");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(550, 300, 450, 300);
		setResizable(false);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(255, 127, 80));
		panel.setBounds(0, 0, 436, 263);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblEnterPlaylistName = new JLabel("Enter PlayList Name");
		lblEnterPlaylistName.setForeground(Color.DARK_GRAY);
		lblEnterPlaylistName.setBounds(124, 65, 203, 25);
		lblEnterPlaylistName.setFont(new Font("Segoe UI Light", Font.ITALIC, 22));
		panel.add(lblEnterPlaylistName);
		
		deletepl = new JTextField();
		deletepl.setColumns(10);
		deletepl.setBackground(new Color(255, 255, 255));
		deletepl.setBounds(123, 124, 204, 41);
		panel.add(deletepl);
		
		JButton btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Class.forName("com.mysql.cj.jdbc.Driver");
					Connection con= DriverManager.getConnection("jdbc:mysql://localhost/","root","rahul123");
					Statement st=con.createStatement();
					PreparedStatement pst=null;
					ResultSet rs=null;
					String qry;
					qry="use beats";
					st.executeUpdate(qry);
					
					qry="SELECT plid from playlists where plname=?;";
					pst=con.prepareStatement(qry);
					pst.setString(1, deletepl.getText());		
					rs=pst.executeQuery();
					rs.next();
					System.out.println(rs.getInt("plid"));
					qry="DELETE FROM plisttosong WHERE plid=? ;";
					pst=con.prepareStatement(qry);
					pst.setInt(1,rs.getInt("plid"));
					pst.executeUpdate();
					qry=" DELETE FROM playlists WHERE plname=? AND username=?;";
					pst=con.prepareStatement(qry);
					pst.setString(1,deletepl.getText());
					pst.setString(2, Name);
					pst.executeUpdate();
					setVisible(false);
					JOptionPane.showMessageDialog(null, "PlayList Deleted Successfully !!");
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (ClassNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
		});
		btnDelete.setFont(new Font("Book Antiqua", Font.BOLD, 18));
		btnDelete.setBounds(164, 194, 111, 34);
		panel.add(btnDelete);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon("C:\\Users\\sai1\\Downloads\\anchor-lee-kO1G3neRA2o-unsplash.jpg"));
		lblNewLabel.setBounds(0, 0, 436, 263);
		panel.add(lblNewLabel);
	}

}
