package bEATSpLAYER;

import java.awt.EventQueue;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JPanel;
import javax.swing.ImageIcon;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;


public class Login {

	  JFrame frmLoginPage;
	private JTextField username;
	private JTextField password;
	private JLabel lblNewLabel_2;
	private JLabel lblNewLabel_3;
	private JPanel panel;
	private JLabel wronglable;

	/**
	 * Launch the application.
	 * @throws UnsupportedLookAndFeelException 
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 * @throws ClassNotFoundException 
	 */
	public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {
		//UIManager.setLookAndFeel("com.jtattoo.plaf.mint.MintLookAndFeel");
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Login window = new Login();
					window.frmLoginPage.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Login() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmLoginPage = new JFrame();
		frmLoginPage.setTitle("Login Page");
		frmLoginPage.getContentPane().setForeground(new Color(255, 255, 255));
		frmLoginPage.setForeground(new Color(0, 0, 0));
		frmLoginPage.setBackground(new Color(0, 0, 0));
		frmLoginPage.setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\sai1\\Downloads\\logo11_25_215410.png"));
		frmLoginPage.getContentPane().setBackground(new Color(255, 255, 255));
		frmLoginPage.getContentPane().setFont(new Font("Tahoma", Font.PLAIN, 10));
		frmLoginPage.setBounds(550, 300, 440, 292);
		frmLoginPage.setResizable(false);
		frmLoginPage.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		panel = new JPanel();
		panel.setBackground(Color.WHITE);
		
		
	
		
		wronglable = new JLabel("");
		wronglable.setBounds(159, 171, 218, 18);
		wronglable.setBackground(new Color(245, 245, 245));
		wronglable.setForeground(new Color(245, 245, 245));
		
		username = new JTextField();
		username.setBounds(159, 88, 218, 25);
		username.setBackground(new Color(255, 255, 255));
		username.setFont(new Font("Book Antiqua", Font.PLAIN, 14));
		username.setColumns(10);
		
		password = new JTextField();
		password.setBounds(159, 137, 218, 25);
		password.setBackground(new Color(255, 255, 255));
		password.setColumns(10);
		
		JButton btnNewButton = new JButton("Login");
		btnNewButton.setBounds(159, 199, 111, 34);
		btnNewButton.setBackground(new Color(255, 255, 255));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String usern = username.getText();
				String pass = password.getText();
				//usern="Rahul Bansode";
				//pass="Rahul123@";
				
				try {
					Class.forName("com.mysql.cj.jdbc.Driver");
					Connection con =DriverManager.getConnection("jdbc:mysql://localhost/","root","rahul123");
					//Object creation & declaration
					Statement st=con.createStatement();
					PreparedStatement pst=null;
					ResultSet rs=null;
					String qry;
					
					qry="CREATE DATABASE IF NOT EXISTS bEATS;";
					st.executeUpdate(qry);
					qry="use bEATS;";
					st.executeUpdate(qry);
					qry="CREATE TABLE IF NOT EXISTS user("
							+ "username varchar(30) not null UNIQUE ,"
							+ "password varchar(30) not null, "
							+ "fname varchar(40) not null,"
							+ "age  int,"
							+ "primary key(username) );";
					st.executeUpdate(qry);

					qry="SELECT username,fname,age ,cast(aes_decrypt(password,'pass76$') as char(100)) from user where username=?;";
					pst=con.prepareStatement(qry);
					pst.setString(1,usern);
				    rs=pst.executeQuery();
				    rs.next();
				    
				    if(rs.getString("username").equalsIgnoreCase(usern))
					{
						if(rs.getString("cast(aes_decrypt(password,'pass76$') as char(100))").equals(pass))
						{
							frmLoginPage.setVisible(false);
							JOptionPane.showMessageDialog(null, "Login Succesfuly");
							homepage hp=new homepage(rs.getString("fname"),rs.getString("username"),rs.getString("age"));
							hp.setVisible(true);
							
							
						}
						else
						{
							
							wronglable.setText("Invalid Username or Password !!");
						}
					}
				   
				    
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				
					
			}
		});
		btnNewButton.setFont(new Font("Segoe UI Light", Font.BOLD, 18));
		
		JLabel lblNewLabel_1 = new JLabel("New User? Sign up >");
		lblNewLabel_1.setBounds(295, 228, 131, 25);
		lblNewLabel_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				frmLoginPage.setVisible(false);
				SignupPage sp=new SignupPage();
				sp.setVisible(true);
								
			}
		});
		lblNewLabel_1.setForeground(Color.LIGHT_GRAY);
		lblNewLabel_1.setFont(new Font("Segoe UI Light", Font.BOLD, 14));
		
		lblNewLabel_2 = new JLabel("UserName");
		lblNewLabel_2.setBounds(54, 81, 95, 34);
		lblNewLabel_2.setForeground(new Color(255, 255, 255));
		lblNewLabel_2.setBackground(new Color(255, 255, 255));
		lblNewLabel_2.setFont(new Font("Segoe UI Light", Font.BOLD, 15));
		
		lblNewLabel_3 = new JLabel("Password");
		lblNewLabel_3.setBounds(54, 132, 95, 28);
		lblNewLabel_3.setForeground(new Color(255, 255, 255));
		lblNewLabel_3.setFont(new Font("Segoe UI Light", Font.BOLD, 15));
		
		JLabel lblNewLabel_4 = new JLabel("");
		lblNewLabel_4.setBounds(0, 0, 436, 263);
		lblNewLabel_4.setIcon(new ImageIcon("C:\\Users\\sai1\\Downloads\\My Post (1).jpg"));
		GroupLayout groupLayout = new GroupLayout(frmLoginPage.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addComponent(panel, GroupLayout.DEFAULT_SIZE, 436, Short.MAX_VALUE)
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addComponent(panel, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 263, Short.MAX_VALUE)
		);
		panel.setLayout(null);
		panel.add(username);
		panel.add(lblNewLabel_3);
		panel.add(password);
		panel.add(wronglable);
		panel.add(btnNewButton);
		panel.add(lblNewLabel_1);
		panel.add(lblNewLabel_2);
		panel.add(lblNewLabel_4);
		frmLoginPage.getContentPane().setLayout(groupLayout);
	}
}
