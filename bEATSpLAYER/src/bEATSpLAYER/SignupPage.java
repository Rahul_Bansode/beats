package bEATSpLAYER;


import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.Toolkit;
import javax.swing.ImageIcon;

public class SignupPage extends JFrame {

	private JPanel contentPane;
	private JTextField Fname;
	private JTextField Usern;
	private JTextField Age;
	private JTextField Pass;
	 private JPanel panel;
	 private JButton btnLoginPage;
	 private JLabel lblNewLabel_3;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SignupPage frame = new SignupPage();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public SignupPage() {
		setTitle("SignUP Page");
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\sai1\\Downloads\\bEATS.png"));
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(550, 300, 441, 292);
		setResizable(false);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 255, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Full Name");
		lblNewLabel.setBounds(40, 55, 97, 25);
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setFont(new Font("Segoe UI Light", Font.BOLD, 15));
		contentPane.add(lblNewLabel);
		
		Fname = new JTextField();
		Fname.setBounds(147, 56, 218, 25);
		Fname.setForeground(Color.BLACK);
		Fname.setBackground(new Color(255, 255, 255));
		contentPane.add(Fname);
		Fname.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("UserName");
		lblNewLabel_1.setBounds(40, 125, 99, 25);
		lblNewLabel_1.setForeground(Color.WHITE);
		lblNewLabel_1.setFont(new Font("Segoe UI Light", Font.BOLD, 15));
		contentPane.add(lblNewLabel_1);
		
		Usern = new JTextField();
		Usern.setBounds(147, 126, 218, 25);
		Usern.setBackground(new Color(255, 255, 255));
		contentPane.add(Usern);
		Usern.setColumns(10);
		
		JLabel lblNewLabel_2 = new JLabel("Age");
		lblNewLabel_2.setBounds(40, 90, 67, 25);
		lblNewLabel_2.setForeground(Color.WHITE);
		lblNewLabel_2.setFont(new Font("Segoe UI Light", Font.BOLD, 15));
		contentPane.add(lblNewLabel_2);
		
		Age = new JTextField();
		Age.setBounds(147, 91, 50, 25);
		Age.setForeground(new Color(0, 0, 0));
		Age.setBackground(new Color(255, 255, 255));
		contentPane.add(Age);
		Age.setColumns(10);
		
		JLabel lblNewLabel_1_1 = new JLabel("Password");
		lblNewLabel_1_1.setBounds(40, 160, 99, 25);
		lblNewLabel_1_1.setForeground(Color.WHITE);
		lblNewLabel_1_1.setFont(new Font("Segoe UI Light", Font.BOLD, 15));
		contentPane.add(lblNewLabel_1_1);
		
		Pass = new JTextField();
		Pass.setBounds(147, 161, 218, 25);
		Pass.setBackground(new Color(255, 255, 255));
		Pass.setColumns(10);
		contentPane.add(Pass);
		
		JButton btnNewButton = new JButton("SignUp");
		btnNewButton.setBounds(147, 208, 111, 34);
		btnNewButton.setBackground(new Color(255, 255, 255));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Class.forName("com.mysql.cj.jdbc.Driver");
					Connection con =DriverManager.getConnection("jdbc:mysql://localhost/","root","rahul123");
					//Object creation & declaration
					Statement st=con.createStatement();
					PreparedStatement pst=null;
					ResultSet rs=null;
					String qry,usern,pass,fname;
					int age;
					usern=Usern.getText();
					pass=Pass.getText();
					fname=Fname.getText();
					age= Integer.parseInt(Age.getText());
					
					qry="CREATE DATABASE IF NOT EXISTS bEATS;";
					st.executeUpdate(qry);
					qry="use bEATS;";
					st.executeUpdate(qry);
					qry="CREATE TABLE IF NOT EXISTS user("
							+ "id int not null auto_increment primary key,"
							+ "username varchar(30) not null UNIQUE,"
							+ "password varchar(30) not null, "
							+ "fname varchar(40) not null,"
							+ "age  int );";
					st.executeUpdate(qry);
					
					qry="insert into user(username,password,fname,age) values (?,aes_encrypt(?,'pass76$'),?,?);";
					pst=con.prepareStatement(qry);
					pst.setString(1,usern);
					pst.setString(2,pass);
					pst.setString(3,fname);
					pst.setInt(4,age);
				    pst.executeUpdate();
				    
				    setVisible(false);
				    JOptionPane.showMessageDialog(null, "SignUp Successfull \nLogin to get started");
				    Login l=new Login();
				    l.frmLoginPage.setVisible(true);
				    
				    		
				    
				    
				} catch (ClassNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
		});
		btnNewButton.setFont(new Font("Book Antiqua", Font.BOLD, 18));
		contentPane.add(btnNewButton);
		
		panel = new JPanel();
		panel.setBounds(0, 0, 436, 263);
		panel.setForeground(new Color(255, 255, 255));
		panel.setBackground(new Color(255, 127, 80));
		contentPane.add(panel);
		panel.setLayout(null);
		
		btnLoginPage = new JButton("LogIn ");
		btnLoginPage.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				Login l=new Login();
			    l.frmLoginPage.setVisible(true);
			}
		});
		btnLoginPage.setFont(new Font("Book Antiqua", Font.BOLD, 18));
		btnLoginPage.setBackground(Color.WHITE);
		btnLoginPage.setBounds(271, 208, 111, 34);
		panel.add(btnLoginPage);
		
		lblNewLabel_3 = new JLabel("");
		lblNewLabel_3.setIcon(new ImageIcon("C:\\Users\\sai1\\Downloads\\My Post (2).jpg"));
		lblNewLabel_3.setBounds(0, 0, 436, 263);
		panel.add(lblNewLabel_3);
	}
}
