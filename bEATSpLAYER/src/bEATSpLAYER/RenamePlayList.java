package bEATSpLAYER;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import java.awt.Font;
import javax.swing.JTextField;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.awt.event.ActionEvent;
import java.awt.Toolkit;
import javax.swing.ImageIcon;

public class RenamePlayList extends JFrame {

	private JPanel RenamePane;
	private JTextField newname;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					RenamePlayList frame = new RenamePlayList("Rahul Bansode" ,"prec" );
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public RenamePlayList(String Name,String SelectedPl) {
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\sai1\\Downloads\\bEATS.png"));
		setTitle("Rename Page");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(550, 300, 450, 300);
		setResizable(false);
		RenamePane = new JPanel();
		RenamePane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(RenamePane);
		RenamePane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(255, 127, 80));
		panel.setBounds(0, 0, 436, 263);
		RenamePane.add(panel);
		panel.setLayout(null);
		
		JLabel lblEnterNewName = new JLabel("Enter New PlayList Name ");
		lblEnterNewName.setForeground(Color.DARK_GRAY);
		lblEnterNewName.setFont(new Font("Segoe UI Light", Font.ITALIC, 22));
		lblEnterNewName.setBounds(107, 55, 269, 43);
		panel.add(lblEnterNewName);
		
		newname = new JTextField();
		newname.setColumns(10);
		newname.setBackground(new Color(255, 255, 255));
		newname.setBounds(122, 108, 204, 41);
		panel.add(newname);
		
		JButton btnRename = new JButton("Rename");
		btnRename.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Class.forName("com.mysql.cj.jdbc.Driver");
					Connection con= DriverManager.getConnection("jdbc:mysql://localhost/","root","rahul123");
					Statement st=con.createStatement();
					PreparedStatement pst=null;
					String qry;
					qry="use beats";
					st.executeUpdate(qry);
					qry="Update playlists SET plname=? WHERE plname=? AND username=?;";
					pst=con.prepareStatement(qry);
					pst.setString(1,newname.getText());
					pst.setString(2, SelectedPl);
					pst.setString(3,Name);
					pst.executeUpdate();
					setVisible(false);
					JOptionPane.showMessageDialog(null, "PlayList Renamed Successfully !!");
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (ClassNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnRename.setFont(new Font("Book Antiqua", Font.BOLD, 18));
		btnRename.setBounds(165, 178, 111, 34);
		panel.add(btnRename);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setForeground(Color.DARK_GRAY);
		lblNewLabel.setIcon(new ImageIcon("C:\\Users\\sai1\\Downloads\\anchor-lee-kO1G3neRA2o-unsplash.jpg"));
		lblNewLabel.setBounds(0, 0, 436, 263);
		panel.add(lblNewLabel);
	}
}
