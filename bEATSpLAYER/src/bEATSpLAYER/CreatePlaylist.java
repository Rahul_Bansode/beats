package bEATSpLAYER;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;



import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.*;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.Toolkit;
import javax.swing.ImageIcon;

public class CreatePlaylist extends JFrame {

	 JPanel CreatePlayist;
	private JTextField Plname;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CreatePlaylist frame = new CreatePlaylist("Rahul bansode");
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws ClassNotFoundException 
	 * @throws SQLException 
	 */
	public CreatePlaylist(String Name) throws ClassNotFoundException, SQLException {
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\sai1\\Downloads\\bEATS.png"));
		setTitle("CreatePlaylist Page");
		
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(550, 300, 450, 300);
		CreatePlayist = new JPanel();
		CreatePlayist.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(CreatePlayist);
		CreatePlayist.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(255, 127, 80));
		panel.setBounds(0, 0, 436, 263);
		CreatePlayist.add(panel);
		panel.setLayout(null);
		
		JLabel lblEnterPlaylistName = new JLabel("Enter PlayList Name");
		lblEnterPlaylistName.setForeground(Color.DARK_GRAY);
		lblEnterPlaylistName.setBounds(124, 64, 202, 25);
		panel.add(lblEnterPlaylistName);
		lblEnterPlaylistName.setFont(new Font("Segoe UI Light", Font.ITALIC, 22));
		
		JButton btnCreate = new JButton("Create");
		btnCreate.setBounds(165, 199, 111, 34);
		panel.add(btnCreate);
		btnCreate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Class.forName("com.mysql.cj.jdbc.Driver");
					Connection con= DriverManager.getConnection("jdbc:mysql://localhost/","root","rahul123");
					Statement st=con.createStatement();
					PreparedStatement pst=null;
					String qry;
					qry="use beats";
					st.executeUpdate(qry);
					qry="Insert into playlists (plname,username) values (?,?) ;";
					pst=con.prepareStatement(qry);
					pst.setString(1,Plname.getText());
					pst.setString(2, Name);
					pst.executeUpdate();
					setVisible(false);
					JOptionPane.showMessageDialog(null, "PlayList Created Successfully !!");
					
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (ClassNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnCreate.setFont(new Font("Book Antiqua", Font.BOLD, 18));
		
		Plname = new JTextField();
		Plname.setBounds(122, 119, 204, 41);
		panel.add(Plname);
		Plname.setBackground(new Color(255, 255, 255));
		Plname.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon("C:\\Users\\sai1\\Downloads\\anchor-lee-kO1G3neRA2o-unsplash.jpg"));
		lblNewLabel.setBounds(0, 0, 436, 263);
		panel.add(lblNewLabel);
	}
}
