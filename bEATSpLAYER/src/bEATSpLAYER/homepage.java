package bEATSpLAYER;


import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.ImageIcon;

import javax.swing.JScrollPane;
import javax.swing.JTable;

import java.awt.Toolkit;
import java.awt.Font;
import java.sql.*;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.ActionEvent;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;



import javazoom.jl.player.advanced.AdvancedPlayer;
import net.proteanit.sql.DbUtils;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.FileInputStream;
import java.awt.SystemColor;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.MatteBorder;
import javax.swing.plaf.basic.BasicOptionPaneUI.ButtonActionListener;
import javax.swing.UIManager;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JTextField;
import javax.swing.RowFilter;
import javax.swing.LayoutStyle.ComponentPlacement;

public class homepage extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private JTable SongList;
	private JTable PlaylistTable;
	private PausablePlayer player;
	DefaultTableModel dm;
	private JTextField searchbar;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					homepage frame = new homepage("a","b","20");
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws ClassNotFoundException 
	 * @throws SQLException 
	 
	 */
	public homepage(String userName,String Name ,String age) throws ClassNotFoundException, SQLException {
		
		player = null;
		Class.forName("com.mysql.cj.jdbc.Driver");
		Connection con= DriverManager.getConnection("jdbc:mysql://localhost/","root","rahul123");
		
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\sai1\\Downloads\\bEATS.png"));
		setTitle(" bEATS 1.0.0");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(0, 0, 1542, 874);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		addWindowListener(new java.awt.event.WindowAdapter() {
		    @Override
		    public void windowClosing(java.awt.event.WindowEvent windowEvent) {
		        if (JOptionPane.showConfirmDialog(contentPane, 
		            "Are you sure you want to close bEATS?", "Close bEATS?", 
		            JOptionPane.YES_NO_OPTION,
		            JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION){
		            System.exit(0);
		            
		        }
		    }
		});
		JPanel Panel1 = new JPanel();
		Panel1.setBorder(null);
		Panel1.setBackground(new Color(255, 127, 80));
		
		JLabel lblNewLabel = new JLabel("User :");
		lblNewLabel.setBounds(10, 212, 52, 45);
		lblNewLabel.setForeground(Color.DARK_GRAY);
		lblNewLabel.setFont(new Font("SansSerif", Font.PLAIN, 18));
		
		JLabel lblNewLabel_1 = new JLabel("Age :");
		lblNewLabel_1.setBounds(10, 255, 52, 45);
		lblNewLabel_1.setForeground(Color.DARK_GRAY);
		lblNewLabel_1.setFont(new Font("SansSerif", Font.PLAIN, 18));
		
		JLabel lblNewLabel_2 = new JLabel("bEATS");
		lblNewLabel_2.setBounds(26, 21, 235, 173);
		lblNewLabel_2.setForeground(Color.DARK_GRAY);
		lblNewLabel_2.setFont(new Font("Tempus Sans ITC", Font.ITALIC, 73));
		lblNewLabel_2.setIcon(null);
		
		JLabel DynamicUser = new JLabel(userName);
		DynamicUser.setBounds(72, 213, 216, 45);
		DynamicUser.setForeground(Color.DARK_GRAY);
		DynamicUser.setFont(new Font("SansSerif", Font.PLAIN, 18));
		
		JLabel lblNewLabel_3 = new JLabel(age);
		lblNewLabel_3.setBounds(72, 255, 60, 45);
		lblNewLabel_3.setForeground(Color.DARK_GRAY);
		lblNewLabel_3.setFont(new Font("SansSerif", Font.PLAIN, 18));
		
		JLabel lblNewLabel_4 = new JLabel("Playlists  \u266B");
		lblNewLabel_4.setBounds(26, 368, 151, 33);
		lblNewLabel_4.setForeground(Color.DARK_GRAY);
		lblNewLabel_4.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				PreparedStatement pst=null;
				ResultSet rs=null;
				try
				{
					Statement st=con.createStatement();
					String qry;
					qry="CREATE DATABASE if not exists beats;";
					st.executeUpdate(qry);
					qry="use beats;";
					st.executeUpdate(qry);
					qry="create table if not exists playlists ("
							+ "plid int not null  auto_increment unique primary key,"
							+ "plname varchar(40) not null ,"
							+ "username varchar(30) not null);";
					st.executeUpdate(qry);
					
					qry="Select plname from playlists where username=? ;";
					pst=con.prepareStatement(qry);
					pst.setString(1,Name);
					rs=pst.executeQuery();
					
				    dm=(DefaultTableModel)PlaylistTable.getModel();
					dm.setRowCount(0);
					
					while(rs.next())
					{
						Object o[]= {rs.getString("plname").toString()};
						dm.addRow(o);
						
					}
					
				}catch(Exception e1)
				{
					System.out.print(e1);
				}
			}
		});
		lblNewLabel_4.setFont(new Font("SansSerif", Font.PLAIN, 18));
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(10, 404, 278, 192);
		scrollPane_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				
			}
		});
		
		PlaylistTable = new JTable();
		PlaylistTable.setForeground(Color.BLACK);
		PlaylistTable.setFont(new Font("Segoe UI Light", Font.PLAIN, 16));
		
		PlaylistTable.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int row=PlaylistTable.getSelectedRow();
				
				String Plliste=(PlaylistTable.getModel().getValueAt(row, 0).toString() );
				System.out.println(Plliste);
				try {
					PreparedStatement pst=null;
					ResultSet rs=null;
					Statement st=con.createStatement();
					String qry;
					int plid;
					qry="use beats;";
					st.executeUpdate(qry);
					qry="select plid from playlists where plname=?";
					pst=con.prepareStatement(qry);
					pst.setString(1,Plliste);
					rs=pst.executeQuery();
					rs.next();
					plid= rs.getInt("plid");
					System.out.println(plid);
					qry="select songs.song_name from songs INNER JOIN plisttosong on plisttosong.suid=songs.suid where plisttosong.plid=?;";
					pst=con.prepareStatement(qry);
					pst.setInt(1,plid);
					rs=pst.executeQuery();
					
					dm=(DefaultTableModel)SongList.getModel();
					dm.setRowCount(0);
					
					
					while(rs.next())
					{
						Object o[]= {rs.getString("songs.song_name")};
						dm.addRow(o);
					}
					
					
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				
				
				
			}
		});
		PlaylistTable.setBackground(Color.WHITE);
		PlaylistTable.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				""
			}
		));
		scrollPane_1.setViewportView(PlaylistTable);
		
		JButton btnNewButton_1 = new JButton("Create ");
		btnNewButton_1.setBounds(10, 640, 84, 45);
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				CreatePlaylist cpl;
				try {
					cpl = new CreatePlaylist(Name);
					cpl.setVisible(true);
				} catch (ClassNotFoundException | SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				
			}
		});
		btnNewButton_1.setForeground(Color.BLACK);
		btnNewButton_1.setBackground(UIManager.getColor("Button.background"));
		btnNewButton_1.setFont(new Font("Book Antiqua", Font.BOLD, 13));
		
		JButton btnNewButton_1_1 = new JButton("Delete");
		btnNewButton_1_1.setBounds(104, 640, 83, 45);
		btnNewButton_1_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				try {
					Class.forName("com.mysql.cj.jdbc.Driver");
					Connection con= DriverManager.getConnection("jdbc:mysql://localhost/","root","rahul123");
					Statement st=con.createStatement();
					PreparedStatement pst=null;
					ResultSet rs=null;
					String qry;
					qry="use beats";
					st.executeUpdate(qry);
					
					qry="SELECT plid from playlists where plname=?;";
					pst=con.prepareStatement(qry);
					pst.setString(1, (String) PlaylistTable.getValueAt(PlaylistTable.getSelectedRow(), PlaylistTable.getSelectedColumn()));		
					rs=pst.executeQuery();
					rs.next();
					System.out.println(rs.getInt("plid"));
					qry="DELETE FROM plisttosong WHERE plid=? ;";
					pst=con.prepareStatement(qry);
					pst.setInt(1,rs.getInt("plid"));
					pst.executeUpdate();
					qry=" DELETE FROM playlists WHERE plname=? AND username=?;";
					pst=con.prepareStatement(qry);
					pst.setString(1,(String) PlaylistTable.getValueAt(PlaylistTable.getSelectedRow(), PlaylistTable.getSelectedColumn()));
					pst.setString(2, Name);
					pst.executeUpdate();
					JOptionPane.showMessageDialog(null,"PlayList Deleted Successfully !!");
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (ClassNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				/*DeletePlayList dpl =new DeletePlayList(Name);
				dpl.setVisible(true);*/
			}
		});
		btnNewButton_1_1.setForeground(Color.BLACK);
		btnNewButton_1_1.setFont(new Font("Book Antiqua", Font.BOLD, 13));
		btnNewButton_1_1.setBackground(UIManager.getColor("Button.background"));
		
		JButton btnNewButton_1_2 = new JButton("Rename ");
		btnNewButton_1_2.setBounds(197, 640, 91, 45);
		btnNewButton_1_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				RenamePlayList rpl=new RenamePlayList(Name,(String) PlaylistTable.getValueAt(PlaylistTable.getSelectedRow(), PlaylistTable.getSelectedColumn()));
				rpl.setVisible(true);
			}
		});
		btnNewButton_1_2.setForeground(Color.BLACK);
		btnNewButton_1_2.setFont(new Font("Book Antiqua", Font.BOLD, 13));
		btnNewButton_1_2.setBackground(UIManager.getColor("Button.background"));
		
		JLabel lblNewLabel_5 = new JLabel("PlayList Oprations");
		lblNewLabel_5.setBounds(10, 588, 167, 42);
		lblNewLabel_5.setForeground(Color.DARK_GRAY);
		lblNewLabel_5.setFont(new Font("SansSerif", Font.PLAIN, 18));
		
		JButton btnNewButton_2 = new JButton("Update");
		btnNewButton_2.setBounds(104, 698, 85, 45);
		btnNewButton_2.setBackground(UIManager.getColor("Button.background"));
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				UpdatePlayList up= new UpdatePlayList((String) PlaylistTable.getValueAt(PlaylistTable.getSelectedRow(), PlaylistTable.getSelectedColumn()));
				up.setVisible(true);
				
				
			}
		});
		btnNewButton_2.setFont(new Font("Book Antiqua", Font.BOLD, 13));
		
		
		
		JPanel panel2 = new JPanel();
		panel2.setBorder(null);
		panel2.setBackground(new Color(255, 127, 80));
		
		JButton btnNewButton_3 = new JButton("");
		btnNewButton_3.setBounds(538, 0, 259, 259);
		btnNewButton_3.setIcon(new ImageIcon("C:\\Users\\sai1\\Downloads\\stop.png"));
		btnNewButton_3.setFont(new Font("Verdana", Font.PLAIN, 79));
		btnNewButton_3.addActionListener(new stopBtnActionListener(this));
		
		JButton btnNewButton_4 = new JButton("");
		btnNewButton_4.setBounds(255, 0, 259, 259);
		btnNewButton_4.setIcon(new ImageIcon("C:\\Users\\sai1\\Downloads\\icons8-circled-play-100 (1).png"));
		btnNewButton_4.setFont(new Font("Verdana", Font.PLAIN, 79));
		btnNewButton_4.addActionListener(new pauseBtnActionListener(this));
		
		JButton btnNewButton_5 = new JButton("");
		btnNewButton_5.setBounds(843, 25, 95, 95);
		btnNewButton_5.setIcon(new ImageIcon("C:\\Users\\sai1\\Downloads\\icons8-circled-play-100 (1).png"));
		btnNewButton_5.setForeground(new Color(0, 0, 0));
		btnNewButton_5.setFont(new Font("Kristen ITC", Font.PLAIN, 79));
		btnNewButton_5.addActionListener(new resumeBtnActionListener(this));
		
		JPanel panel = new JPanel();
		panel.setBorder(null);
		panel.setBackground(new Color(255, 127, 80));
		
		
		JButton btnNewButton = new JButton("Songs                                                                                                                                                                                                                                                                                                                                                                          \u2193");
		btnNewButton.setBounds(0, 0, 1235, 21);
		btnNewButton.setFont(new Font("Book Antiqua", Font.BOLD, 12));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					
					Statement st = con.createStatement();
					PreparedStatement pst=null;
					ResultSet rs=null;
					String qry;
					
					qry="CREATE DATABASE IF NOT EXISTS bEATS ";
					st.executeUpdate(qry);
					
					
					qry="USE bEATS";
					st.execute(qry);
					qry ="CREATE TABLE IF NOT EXISTS songs"
							+ "(suid double(3,3) NOT NULL ,"
							+ "song_name varchar(255) unique NOT NULL,"
							+ "path varchar(255) NOT NULL,"
							+ "PRIMARY KEY(suid) );";
					st.executeUpdate(qry);
					
					qry="SELECT suid,song_name from songs;";
					pst=con.prepareStatement(qry);
					rs=pst.executeQuery();
					
					dm=(DefaultTableModel)SongList.getModel();
					dm.setRowCount(0);
					
					
					while(rs.next())
					{
						Object o[]= {rs.getString("song_name")};
						dm.addRow(o);
					}
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} 
				
			}
		});
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(0, 20, 1235, 508);
		
		SongList = new JTable();
		SongList.setShowGrid(false);
		SongList.setForeground(Color.BLACK);
		SongList.setFont(new Font("Segoe UI Light", Font.ITALIC, 18));
		SongList.setBackground(Color.WHITE);
		SongList.setRowHeight(40);
		SongList.addMouseListener(new MouseListenerClass(this));/*new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int row=SongList.getSelectedRow();
				
				String SongName=(SongList.getModel().getValueAt(row, 0).toString() );
				System.out.print(SongName);
				try
				{
					Statement st=con.createStatement();
					PreparedStatement pst=null;
					ResultSet rs=null;
					String qry;
					
					qry="CREATE DATABASE IF NOT EXISTS bEATS ";
					st.executeUpdate(qry);
					qry="USE bEATS";
					st.execute(qry);
					qry="SELECT Song_name,path from songs where Song_name=?;";
					pst=con.prepareStatement(qry);
					pst.setString(1, SongName);
					rs=pst.executeQuery();
					rs.next();
					
					//FileInputStream fp=new FileInputStream(rs.getString("path"));
					AdvancedPlayer ap=new AdvancedPlayer(fp);
					ap.play();//
					
		            FileInputStream input = new FileInputStream(rs.getString("path"));
		            if(player != null)
		            {
		            	System.out.println("@@@"+ rs.getString("path"));
		            	player.close();
		            }
		            	
		            PausablePlayer player = new PausablePlayer(input);
		            player.play();
				}
				catch(Exception e1)
				{
					System.out.println(e1);
					
				}
			}
		});*/
		SongList.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				""
			}
		) {
			Class[] columnTypes = new Class[] {
				String.class
			};
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
		});
		SongList.getColumnModel().getColumn(0).setPreferredWidth(963);
		scrollPane.setViewportView(SongList);
		try {
			
			Statement st = con.createStatement();
			PreparedStatement pst=null;
			ResultSet rs=null;
			String qry;
			
			qry="CREATE DATABASE IF NOT EXISTS bEATS ";
			st.executeUpdate(qry);
			
			
			qry="USE bEATS";
			st.execute(qry);
			qry ="CREATE TABLE IF NOT EXISTS songs"
					+ "(suid double(3,3) NOT NULL ,"
					+ "song_name varchar(255) unique NOT NULL,"
					+ "path varchar(255) NOT NULL,"
					+ "PRIMARY KEY(suid) );";
			st.executeUpdate(qry);
			
			qry="SELECT suid,song_name from songs;";
			pst=con.prepareStatement(qry);
			rs=pst.executeQuery();
			
			dm=(DefaultTableModel)SongList.getModel();
			dm.setRowCount(0);
			
			
			while(rs.next())
			{
				Object o[]= {rs.getString("song_name")};
				dm.addRow(o);
			}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(Panel1, GroupLayout.DEFAULT_SIZE, 300, Short.MAX_VALUE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(panel2, GroupLayout.PREFERRED_SIZE, 1226, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(297)
							.addComponent(panel, GroupLayout.PREFERRED_SIZE, 1235, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap())
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
						.addComponent(Panel1, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 808, Short.MAX_VALUE)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addContainerGap(12, Short.MAX_VALUE)
							.addComponent(panel, GroupLayout.PREFERRED_SIZE, 530, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(panel2, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)))
					.addGap(19))
		);
		panel.setLayout(null);
		panel.add(btnNewButton);
		panel.add(scrollPane);
		panel2.setLayout(null);
		panel2.add(btnNewButton_4);
		panel2.add(btnNewButton_3);
		panel2.add(btnNewButton_5);
		
		JButton btnNewButton_6 = new JButton("\uD83D\uDD0D");
		btnNewButton_6.setBounds(10, 10, 43, 27);
		btnNewButton_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String tf=searchbar.getText();
				try
				{
					Class.forName("com.mysql.cj.jdbc.Driver");
					Connection con= DriverManager.getConnection("jdbc:mysql://localhost/","root","rahul123");
					Statement st=con.createStatement();
					PreparedStatement pst=null;
					ResultSet rs=null;
					String qry;
					
					qry="CREATE DATABASE IF NOT EXISTS bEATS ";
					st.executeUpdate(qry);
					qry="USE bEATS";
					st.execute(qry);
					//qry="SELECT Song_name,path from songs where Song_name LIKE '%"+tf+"%'";
					qry="SELECT Song_name,path from songs where Song_name LIKE ?";
					pst=con.prepareStatement(qry);
					pst.setString(1, "%" + tf + "%");
					rs=pst.executeQuery();
					//rs.next();
					//SongList.setModel(DbUtils.resultSetToTableModel(rs));
					dm=(DefaultTableModel)SongList.getModel();
					dm.setRowCount(0);
					while(rs.next())
					{
						Object o[]= {rs.getString("songs.song_name")};
						dm.addRow(o);
					}
				}catch(Exception e3)
				{
					System.out.print(e3);
				}
			}
		});
		
		panel2.add(btnNewButton_6);
		
		searchbar = new JTextField();
		searchbar.setBounds(53, 10, 133, 27);
		
		panel2.add(searchbar);
		searchbar.setColumns(10);
		
		JLabel lblNewLabel_7 = new JLabel("");
		lblNewLabel_7.setBounds(-11, 0, 1246, 259);
		lblNewLabel_7.setIcon(new ImageIcon("C:\\Users\\sai1\\Downloads\\anchor-lee-kO1G3neRA2o-unsplash.jpg"));
		panel2.add(lblNewLabel_7);
		
		JLabel lblNewLabel_8 = new JLabel("New label");
		lblNewLabel_8.setBounds(62, 133, 45, 13);
		panel2.add(lblNewLabel_8);
		Panel1.setLayout(null);
		Panel1.add(lblNewLabel);
		Panel1.add(btnNewButton_1_1);
		Panel1.add(DynamicUser);
		Panel1.add(lblNewLabel_1);
		Panel1.add(btnNewButton_2);
		Panel1.add(lblNewLabel_4);
		Panel1.add(btnNewButton_1);
		Panel1.add(lblNewLabel_2);
		Panel1.add(lblNewLabel_3);
		Panel1.add(lblNewLabel_5);
		Panel1.add(scrollPane_1);
		Panel1.add(btnNewButton_1_2);
		
		JButton btnNewButton_1_3 = new JButton("SignOut");
		btnNewButton_1_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				 if (JOptionPane.showConfirmDialog(contentPane, 
				            "Are you sure you want to SignOut?", "SignOut bEATS?", 
				            JOptionPane.YES_NO_OPTION,
				            JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION){
					         player.close();
					 		setVisible(false);
					 		Login l=new Login();
					 		l.frmLoginPage.setVisible(true);
				            
				        }

				
			}
		});
		btnNewButton_1_3.setForeground(Color.BLACK);
		btnNewButton_1_3.setFont(new Font("Book Antiqua", Font.BOLD, 13));
		btnNewButton_1_3.setBackground(SystemColor.menu);
		btnNewButton_1_3.setBounds(177, 310, 84, 33);
		Panel1.add(btnNewButton_1_3);
		
		JButton btnNewButton_1_3_1 = new JButton("Delete Account");
		btnNewButton_1_3_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (JOptionPane.showConfirmDialog(contentPane, 
			            "Are you sure you want to Delete account?", "Delete Account ?", 
			            JOptionPane.YES_NO_OPTION,
			            JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION)
					{
					try {
						Class.forName("com.mysql.cj.jdbc.Driver");
						Connection con =DriverManager.getConnection("jdbc:mysql://localhost/","root","rahul123");
						//Object creation & declaration
						Statement st=con.createStatement();
						PreparedStatement pst=null;
						ResultSet rs=null;
						String qry;
						qry="use beats;";
						st.executeUpdate(qry);
						qry="DELETE FROM user WHERE username=?";
						pst=con.prepareStatement(qry);
						pst.setString(1,Name);
						pst.executeUpdate();
						JOptionPane.showMessageDialog(null,"Account Deleted Successfully");
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
						player.close();
				 		setVisible(false);
				 		Login l=new Login();
				 		l.frmLoginPage.setVisible(true);
			            
			        }
				
			}
		});
		btnNewButton_1_3_1.setForeground(Color.BLACK);
		btnNewButton_1_3_1.setFont(new Font("Book Antiqua", Font.BOLD, 13));
		btnNewButton_1_3_1.setBackground(SystemColor.menu);
		btnNewButton_1_3_1.setBounds(10, 310, 135, 33);
		Panel1.add(btnNewButton_1_3_1);
		
		JLabel lblNewLabel_6 = new JLabel("");
		lblNewLabel_6.setIcon(new ImageIcon("C:\\Users\\sai1\\Downloads\\anchor-lee-kO1G3neRA2o-unsplash.jpg"));
		lblNewLabel_6.setBounds(0, 0, 298, 836);
		Panel1.add(lblNewLabel_6);
		contentPane.setLayout(gl_contentPane);
		
		
		Panel1.revalidate();
		Panel1.repaint();
	}
	
	public class stopBtnActionListener implements ActionListener{
		private homepage uiclass;
		stopBtnActionListener(homepage uiclass)
		{
			this.uiclass = uiclass;
		}
		public void actionPerformed(ActionEvent e) {
			uiclass.player.close();
		}
	}
	
	public class pauseBtnActionListener implements ActionListener{
		private homepage uiclass;
		pauseBtnActionListener(homepage uiclass)
		{
			this.uiclass = uiclass;
		}
		public void actionPerformed(ActionEvent e) {
			uiclass.player.pause();
		}
	}
	
	public class resumeBtnActionListener implements ActionListener{
		private homepage uiclass;
		resumeBtnActionListener(homepage uiclass)
		{
			this.uiclass = uiclass;
		}
		public void actionPerformed(ActionEvent e) {
			uiclass.player.resume();
		}
	}
	public class MouseListenerClass extends MouseAdapter {
	    private homepage uiclass;

	    public MouseListenerClass(homepage uiclass) {
	        this.uiclass = uiclass;
	    }
	    
		@Override
		public void mouseClicked(MouseEvent e) {
			int row=SongList.getSelectedRow();
			
			String SongName=(SongList.getModel().getValueAt(row, 0).toString() );
			System.out.print(SongName);
			String tf=searchbar.getText();
			try
			{
				Class.forName("com.mysql.cj.jdbc.Driver");
				Connection con= DriverManager.getConnection("jdbc:mysql://localhost/","root","rahul123");
				Statement st=con.createStatement();
				PreparedStatement pst=null;
				ResultSet rs=null;
				String qry;
				
				qry="CREATE DATABASE IF NOT EXISTS bEATS ";
				st.executeUpdate(qry);
				qry="USE bEATS";
				st.execute(qry);
				qry="SELECT Song_name,path from songs where Song_name=?;";
				pst=con.prepareStatement(qry);
				pst.setString(1, SongName);
				rs=pst.executeQuery();
				rs.next();
				
				/*FileInputStream fp=new FileInputStream(rs.getString("path"));
				AdvancedPlayer ap=new AdvancedPlayer(fp);
				ap.play();*/
				
	            FileInputStream input = new FileInputStream(rs.getString("path"));
	            if(uiclass.player != null)
	            {
	            	System.out.println("@@@"+ rs.getString("path"));
	            	player.close();
	            }
	            	
	            uiclass.player = new PausablePlayer(input);
	            player.play();
			}
			catch(Exception e1)
			{
				System.out.println(e1);
				
			}
		}
	}
}
