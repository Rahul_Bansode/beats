package bEATSpLAYER;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Color;
import javax.swing.JButton;
import java.awt.Toolkit;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;

public class UpdatePlayList extends JFrame {

	private JPanel contentPane;
	private JTable favsongs;
	private JTable allsongs;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UpdatePlayList frame = new UpdatePlayList(" test");
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public UpdatePlayList(String SelectedPl) {
		
		setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\sai1\\Downloads\\bEATS.png"));
		setTitle("PlayList Updation Page");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(450,100, 765, 515);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(56, 65, 267, 292);
		contentPane.add(scrollPane);
		
		favsongs = new JTable();
		favsongs.setForeground(Color.BLACK);
		favsongs.setFont(new Font("Book Antiqua", Font.PLAIN, 13));
		favsongs.setBackground(Color.WHITE);
		favsongs.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Songs"
			}
		));
		scrollPane.setViewportView(favsongs);
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection con =DriverManager.getConnection("jdbc:mysql://localhost/","root","rahul123");
			Statement st = con.createStatement();
			PreparedStatement pst=null;
			ResultSet rs=null;
			String qry;
			
			qry="use beats;";
			st.executeUpdate(qry);
			qry="SELECT plid from playlists where plname=?;";
			pst=con.prepareStatement(qry);
			pst.setString(1,SelectedPl);		
			rs=pst.executeQuery();
			rs.next();
			int PLID= rs.getInt("plid");
			System.out.println(PLID);
			qry="select songs.song_name from songs INNER JOIN plisttosong on plisttosong.suid=songs.suid where plisttosong.plid=?;";
			pst=con.prepareStatement(qry);
			pst.setInt(1,PLID);
			rs=pst.executeQuery();
			
			DefaultTableModel dm=(DefaultTableModel)favsongs.getModel();
			dm.setRowCount(0);
			
			
			while(rs.next())
			{
				Object o[]= {rs.getString("songs.song_name")};
				dm.addRow(o);
			}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(423, 63, 265, 293);
		contentPane.add(scrollPane_1);
		
		allsongs = new JTable();
		allsongs.setForeground(Color.BLACK);
		allsongs.setFont(new Font("Book Antiqua", Font.PLAIN, 13));
		allsongs.setBackground(Color.WHITE);
		allsongs.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Songs"
			}
		));
		scrollPane_1.setViewportView(allsongs);
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection con =DriverManager.getConnection("jdbc:mysql://localhost/","root","rahul123");
			Statement st = con.createStatement();
			PreparedStatement pst=null;
			ResultSet rs=null;
			String qry;
			
			qry="CREATE DATABASE IF NOT EXISTS bEATS ";
			st.executeUpdate(qry);
			
			
			qry="USE bEATS";
			st.execute(qry);
			qry="SELECT suid,song_name from songs;";
			pst=con.prepareStatement(qry);
			rs=pst.executeQuery();
			
			DefaultTableModel dm=(DefaultTableModel)allsongs.getModel();
			dm.setRowCount(0);
			
			
			while(rs.next())
			{
				Object o[]= {rs.getString("song_name")};
				dm.addRow(o);
			}
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		JLabel lblNewLabel = new JLabel("Playlist Songs");
		lblNewLabel.setForeground(Color.DARK_GRAY);
		lblNewLabel.setBounds(56, 24, 165, 29);
		lblNewLabel.setFont(new Font("Segoe UI Light", Font.ITALIC, 21));
		contentPane.add(lblNewLabel);
		
		JLabel lblServerSideSongs = new JLabel("Server Side Songs");
		lblServerSideSongs.setForeground(Color.DARK_GRAY);
		lblServerSideSongs.setBounds(421, 24, 175, 29);
		lblServerSideSongs.setFont(new Font("Segoe UI Light", Font.ITALIC, 21));
		contentPane.add(lblServerSideSongs);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(255, 127, 80));
		panel.setBounds(0, 0, 751, 478);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JButton btnNewButton = new JButton("Delete");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Class.forName("com.mysql.cj.jdbc.Driver");
					Connection con= DriverManager.getConnection("jdbc:mysql://localhost/","root","rahul123");
					Statement st=con.createStatement();
					PreparedStatement pst=null;
					ResultSet rs=null;
					String qry;
					qry="use beats";
					st.executeUpdate(qry);
					System.out.println(SelectedPl);
					qry="SELECT plid from playlists where plname=?;";
					pst=con.prepareStatement(qry);
					pst.setString(1,SelectedPl);		
					rs=pst.executeQuery();
					rs.next();
					int PLID= rs.getInt("plid");
					System.out.println(PLID);
					 
					
					DefaultTableModel model1=(DefaultTableModel)favsongs.getModel();
					//TableModel model1 =favsongs.getModel();
					  int indexs[]= favsongs.getSelectedRows();
					  Object [] row =new Object[1];
					 
					  for(int i=indexs.length-1;i>=0;i--)
					  {
						  row[0]=model1.getValueAt(indexs[i], 0);
						  
						  qry="SELECT suid FROM songs WHERE song_name=?";
						  pst=con.prepareStatement(qry);
						  pst.setString(1, (String) row[0]);
						  rs=pst.executeQuery();
						  rs.next();
						  System.out.println(rs.getDouble("suid"));
						  qry="DELETE FROM plisttosong WHERE plid=? AND suid=? ;";
						  pst=con.prepareStatement(qry);
						  pst.setInt(1,PLID);
						  pst.setDouble(2,rs.getDouble("suid"));
						  pst.executeUpdate();
						  System.out.println(row[0]);
						  System.out.println();
						  model1.removeRow(indexs[i]);
					  }
					
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (ClassNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
		});
		btnNewButton.setFont(new Font("Book Antiqua", Font.BOLD, 18));
		btnNewButton.setBounds(255, 398, 100, 35);
		panel.add(btnNewButton);
		
		JButton btnAdd = new JButton("Add");
		btnAdd.setIcon(null);
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			  
				try {
					Class.forName("com.mysql.cj.jdbc.Driver");
					Connection con= DriverManager.getConnection("jdbc:mysql://localhost/","root","rahul123");
					Statement st=con.createStatement();
					PreparedStatement pst=null;
					ResultSet rs=null;
					String qry;
					qry="use beats";
					st.executeUpdate(qry);
					System.out.println(SelectedPl);
					qry="SELECT plid from playlists where plname=?;";
					pst=con.prepareStatement(qry);
					pst.setString(1,SelectedPl);		
					rs=pst.executeQuery();
					rs.next();
					int PLID= rs.getInt("plid");
					System.out.println(PLID);
					 TableModel model1 =allsongs.getModel();
					  int indexs[]=allsongs.getSelectedRows();
					
					  
					  Object [] row =new Object[1];
					  DefaultTableModel model2=(DefaultTableModel)favsongs.getModel();
					  for(int i=0;i<indexs.length;i++)
					  {
						  row[0]=model1.getValueAt(indexs[i], 0);
						  qry="SELECT suid FROM songs WHERE song_name=?";
						  pst=con.prepareStatement(qry);
						  pst.setString(1, (String) row[0]);
						  rs=pst.executeQuery();
						  rs.next();
						  System.out.println(rs.getDouble("suid"));
						  qry="INSERT INTO plisttosong (plid,suid) values(?,?) ;";
						  pst=con.prepareStatement(qry);
						  pst.setInt(1,PLID);
						  pst.setDouble(2,rs.getDouble("suid"));
						  pst.executeUpdate();
						  System.out.println(row[0]);
						  System.out.println();
						  model2.addRow(row);
					  }
					  allsongs.clearSelection();
					
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (ClassNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			 
			}
			
		});
		btnAdd.setFont(new Font("Book Antiqua", Font.BOLD, 18));
		btnAdd.setBounds(393, 398, 100, 35);
		panel.add(btnAdd);
		
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setIcon(new ImageIcon("C:\\Users\\sai1\\Downloads\\anchor-lee-kO1G3neRA2o-unsplash.jpg"));
		lblNewLabel_1.setBounds(0, 0, 751, 478);
		panel.add(lblNewLabel_1);
	}
}
