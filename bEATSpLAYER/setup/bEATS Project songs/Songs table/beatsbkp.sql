-- MySQL dump 10.13  Distrib 5.7.32, for Win64 (x86_64)
--
-- Host: localhost    Database: beats
-- ------------------------------------------------------
-- Server version	5.7.32-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `songs`
--

DROP TABLE IF EXISTS `songs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `songs` (
  `suid` double(3,3) NOT NULL,
  `song_name` varchar(255) NOT NULL,
  `path` varchar(255) NOT NULL,
  PRIMARY KEY (`suid`),
  UNIQUE KEY `song_name` (`song_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `songs`
--

LOCK TABLES `songs` WRITE;
/*!40000 ALTER TABLE `songs` DISABLE KEYS */;
INSERT INTO `songs` VALUES (0.134,'Makhna','E:\\bEATS Project songs\\Makhna.mp3'),(0.186,'Imaginary','E:\\bEATS Project songs\\Imaginary.mp3'),(0.213,'Aashiyan','E:\\bEATS Project songs\\Aashiyan.mp3'),(0.215,'Khari','E:\\bEATS Project songs\\Khari.mp3'),(0.238,'Dheeme Dheeme','E:\\bEATS Project songs\\Dheeme Dheeme.mp3'),(0.335,'BamBholle','E:\\bEATS Project songs\\BamBholle.mp3'),(0.461,'Zaalima','E:\\bEATS Project songs\\Zaalima.mp3'),(0.516,'Lahore','E:\\bEATS Project songs\\Lahore.mp3'),(0.611,'Ude Dil Befikre','E:\\bEATS Project songs\\Ude Dil Befikre.mp3'),(0.710,'Urvashi','E:\\bEATS Project songs\\Urvashi.mp3'),(0.718,'War Theme','E:\\bEATS Project songs\\War Theme.mp3'),(0.862,'Naach Meri Rani','E:\\bEATS Project songs\\Naach Meri Rani.mp3'),(0.905,'Tere Chehre Se Nazar Nahin','E:\\bEATS Project songs\\Tere Chehre Se Nazar Nahin.mp3'),(0.908,'Apna Time Aayega','E:\\bEATS Project songs\\Apna Time Aayega.mp3'),(0.936,'Made In India','E:\\bEATS Project songs\\Made In India.mp3'),(0.940,'Tere Te','E:\\bEATS Project songs\\Tere Te.mp3'),(0.987,'Tere Wargi Nai Ae','E:\\bEATS Project songs\\Tere Wargi Nai Ae.mp3');
/*!40000 ALTER TABLE `songs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'Rahul Bansode','.&_ìcÅïüÕÍ”,n©%+'),(2,'Rushikesh Bansode','Øåû\"F·Ž8ãv¿ÐÚ'),(3,'Tejas Dalvi','~äšeÛF©èZ ¾³;˜');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-11-18 13:44:14
