-- MySQL dump 10.13  Distrib 5.7.32, for Win64 (x86_64)
--
-- Host: localhost    Database: beats
-- ------------------------------------------------------
-- Server version	5.7.32-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `playlists`
--

DROP TABLE IF EXISTS `playlists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `playlists` (
  `plid` int(11) NOT NULL AUTO_INCREMENT,
  `plname` varchar(40) NOT NULL,
  `username` varchar(30) NOT NULL,
  PRIMARY KEY (`plid`),
  UNIQUE KEY `plid` (`plid`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `playlists`
--

LOCK TABLES `playlists` WRITE;
/*!40000 ALTER TABLE `playlists` DISABLE KEYS */;
INSERT INTO `playlists` VALUES (50,'Rahul','Rahul Bansode');
/*!40000 ALTER TABLE `playlists` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `plisttosong`
--

DROP TABLE IF EXISTS `plisttosong`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plisttosong` (
  `plid` int(11) NOT NULL,
  `suid` double(3,3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `plisttosong`
--

LOCK TABLES `plisttosong` WRITE;
/*!40000 ALTER TABLE `plisttosong` DISABLE KEYS */;
INSERT INTO `plisttosong` VALUES (50,0.213),(50,0.561),(50,0.984),(50,0.908),(50,0.335),(50,0.682),(50,0.556),(50,0.267),(50,0.335);
/*!40000 ALTER TABLE `plisttosong` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `songs`
--

DROP TABLE IF EXISTS `songs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `songs` (
  `suid` double(3,3) NOT NULL,
  `song_name` varchar(255) NOT NULL,
  `path` varchar(255) NOT NULL,
  PRIMARY KEY (`suid`),
  UNIQUE KEY `song_name` (`song_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `songs`
--

LOCK TABLES `songs` WRITE;
/*!40000 ALTER TABLE `songs` DISABLE KEYS */;
INSERT INTO `songs` VALUES (0.021,'Loca','E:\\bEATS Project songs\\Loca.mp3'),(0.052,'Surma Surma','E:\\bEATS Project songs\\Surma Surma.mp3'),(0.075,'Khairiyat','E:\\bEATS Project songs\\Khairiyat.mp3'),(0.134,'Makhna','E:\\bEATS Project songs\\Makhna.mp3'),(0.186,'Imaginary','E:\\bEATS Project songs\\Imaginary.mp3'),(0.200,'Coca Cola','E:\\bEATS Project songs\\Coca Cola.mp3'),(0.213,'Aashiyan','E:\\bEATS Project songs\\Aashiyan.mp3'),(0.215,'Khari','E:\\bEATS Project songs\\Khari.mp3'),(0.226,'Beautifull Akon','E:\\bEATS Project songs\\Beautifull Akon.mp3'),(0.228,'Guitar Sikhda','E:\\bEATS Project songs\\Guitar Sikhda.mp3'),(0.237,'Humnava Mere','E:\\bEATS Project songs\\Humnava Mere.mp3'),(0.238,'Dheeme Dheeme','E:\\bEATS Project songs\\Dheeme Dheeme.mp3'),(0.258,'Raat Kamaal Hai','E:\\bEATS Project songs\\Raat Kamaal Hai.mp3'),(0.267,'Kamaal','E:\\bEATS Project songs\\Kamaal.mp3'),(0.302,'Enna Sona','E:\\bEATS Project songs\\Enna Sona.mp3'),(0.335,'BamBholle','E:\\bEATS Project songs\\BamBholle.mp3'),(0.356,'Nikle Current','E:\\bEATS Project songs\\Nikle Current.mp3'),(0.422,'Billo Tu Agg Hai','E:\\bEATS Project songs\\Billo Tu Agg Hai.mp3'),(0.433,'BurjKhalifa','E:\\bEATS Project songs\\BurjKhalifa.mp3'),(0.460,'Gazab Ka Hai Din','E:\\bEATS Project songs\\Gazab Ka Hai Din.mp3'),(0.461,'Zaalima','E:\\bEATS Project songs\\Zaalima.mp3'),(0.477,'Friends','E:\\bEATS Project songs\\Friends.mp3'),(0.501,'Idhazhin Oram','E:\\bEATS Project songs\\Idhazhin Oram.mp3'),(0.516,'Lahore','E:\\bEATS Project songs\\Lahore.mp3'),(0.527,'Superstar','E:\\bEATS Project songs\\Superstar.mp3'),(0.556,'Issey Kehte Hai Hip Hop','E:\\bEATS Project songs\\Issey Kehte Hai Hip Hop.mp3'),(0.561,'Agar Tum Saath Ho','E:\\bEATS Project songs\\Agar Tum Saath Ho.mp3'),(0.603,'Paagal','E:\\bEATS Project songs\\Paagal.mp3'),(0.611,'Ude Dil Befikre','E:\\bEATS Project songs\\Ude Dil Befikre.mp3'),(0.641,'First Kiss','E:\\bEATS Project songs\\First Kiss.mp3'),(0.682,'Yalgaar','E:\\bEATS Project songs\\Yalgaar.mp3'),(0.710,'Urvashi','E:\\bEATS Project songs\\Urvashi.mp3'),(0.718,'War Theme','E:\\bEATS Project songs\\War Theme.mp3'),(0.726,'Mirchi','E:\\bEATS Project songs\\Mirchi.mp3'),(0.758,'Malang','E:\\bEATS Project songs\\Malang.mp3'),(0.787,'Jaan Ban Gaye','E:\\bEATS Project songs\\Jaan Ban Gaye.mp3'),(0.796,'Ik Mulaqaat','E:\\bEATS Project songs\\Ik Mulaqaat.mp3'),(0.810,'Raabta','E:\\bEATS Project songs\\Raabta.mp3'),(0.860,'Soch Na Sake','E:\\bEATS Project songs\\Soch Na Sake.mp3'),(0.862,'Naach Meri Rani','E:\\bEATS Project songs\\Naach Meri Rani.mp3'),(0.869,'Genda Phool','E:\\bEATS Project songs\\Genda Phool.mp3'),(0.900,'Care Ni Karda','E:\\bEATS Project songs\\Care Ni Karda.mp3'),(0.905,'Tere Chehre Se Nazar Nahin','E:\\bEATS Project songs\\Tere Chehre Se Nazar Nahin.mp3'),(0.908,'Apna Time Aayega','E:\\bEATS Project songs\\Apna Time Aayega.mp3'),(0.910,'Filhall','E:\\bEATS Project songs\\Filhall.mp3'),(0.931,'Qaafirana','E:\\bEATS Project songs\\Qaafirana.mp3'),(0.936,'Made In India','E:\\bEATS Project songs\\Made In India.mp3'),(0.940,'Tere Te','E:\\bEATS Project songs\\Tere Te.mp3'),(0.948,'Pal Pal Dil Ke Paas','E:\\bEATS Project songs\\Pal Pal Dil Ke Paas.mp3'),(0.967,'Ghungroo','E:\\bEATS Project songs\\Ghungroo.mp3'),(0.975,'Kar Gayi Chull','E:\\bEATS Project songs\\Kar Gayi Chull.mp3'),(0.984,'All Black','E:\\bEATS Project songs\\All Black.mp3'),(0.987,'Tere Wargi Nai Ae','E:\\bEATS Project songs\\Tere Wargi Nai Ae.mp3'),(0.998,'Ishq Tera','E:\\bEATS Project songs\\Ishq Tera.mp3');
/*!40000 ALTER TABLE `songs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `username` varchar(30) NOT NULL,
  `password` varchar(30) NOT NULL,
  `fname` varchar(40) NOT NULL,
  `age` int(11) DEFAULT NULL,
  PRIMARY KEY (`username`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES ('Bhushan Bansode','‡Ù0†´{·TwŠ„ÜÅ•0','Bhushan Sanjay Bansode',30),('Rahul Bansode','.&_ìcÅïüÕÍ”,n©%+','Rahul Rajendra Bansode',20),('rushi bansode','múU»\ZU^XõœU…','Rushikesh sanjay bansode',28);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-12-07 12:37:53
